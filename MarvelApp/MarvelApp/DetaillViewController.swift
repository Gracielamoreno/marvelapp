//
//  DetaillViewController.swift
//  MarvelApp
//
//  Created by Graciela Moreno on 3/1/18.
//  Copyright © 2018 Graciela Moreno. All rights reserved.
//

import UIKit

class DetaillViewController: UIViewController {
    
    @IBOutlet weak var pokemonImageview: UIImageView!
    var pokemon:Pokemon?
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        print (pokemon?.name)
          self.title = pokemon?.name
        let pkService =  PokemonService()
        pkService.getPokemonImage(id: (pokemon?.id)!) {(pkImage) in
            self.pokemonImageview.image = pkImage
            
        }
    }
    
  

}
