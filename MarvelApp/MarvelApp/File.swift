//
//  File.swift
//  MarvelApp
//
//  Created by Graciela Moreno on 29/11/17.
//  Copyright © 2017 Graciela Moreno. All rights reserved.
//

import Foundation
import ObjectMapper

class Pokemon: Mappable {
    var name:String?
    var heigth:Double?
    var weight:Double?
    var id:Int?

    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        heigth <- map["height"]
        weight <- map["weigt"]
        
    }
    
    
}
