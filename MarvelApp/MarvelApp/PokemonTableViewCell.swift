//
//  PokemonTableViewCell.swift
//  MarvelApp
//
//  Created by Graciela Moreno on 3/1/18.
//  Copyright © 2018 Graciela Moreno. All rights reserved.
//

import UIKit

class PokemonTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var weigthLabel: UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
   
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func fillData (pokemon:Pokemon){
        nameLabel.text = pokemon.name
        heightLabel.text = "\(pokemon.heigth ?? 0)"
        weigthLabel.text = "\(pokemon.weight ?? 0)"
        
    }
}
