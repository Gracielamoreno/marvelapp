//
//  PokemosnService.swift
//  MarvelApp
//
//  Created by Graciela Moreno on 2/1/18.
//  Copyright © 2018 Graciela Moreno. All rights reserved.
//
import UIKit
import Alamofire
import AlamofireObjectMapper
import Foundation
import AlamofireImage

protocol PokemonServiceDelegate  {
    func get20firstpokemos(pokemons:[Pokemon])
}
class PokemonService {
    
    var delegate:PokemonServiceDelegate?
    
    func downloadpokemon(){
        var pokemonArray:[Pokemon] = []
        let dpGR = DispatchGroup()
        for i in 1...3 {
            dpGR.enter()
         Alamofire.request("https://pokeapi.co/api/v2/pokemon/\(i)").responseObject  {(response:DataResponse< Pokemon >) in
        
        let pokemon = response.result.value
            pokemonArray.append(pokemon!)
        dpGR.leave()
            }
         }
        dpGR.notify(queue: .main){
             let sortedArray = pokemonArray.sorted(by: {$0.id! < $1.id!})
             self.delegate?.get20firstpokemos(pokemons: sortedArray)
      
      }
    }
    
    func getPokemonImage(id:Int, competion:@escaping (UIImage)->()){
        let url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/\(id).png"
        
        Alamofire.request(url).responseImage { response in debugPrint(response)
            if let image = response.result.value{
                competion(image)
            }
        }
    }
}

