//
//  PokexviewControler.swift
//  MarvelApp
//
//  Created by Graciela Moreno on 29/11/17.
//  Copyright © 2017 Graciela Moreno. All rights reserved.
//

import Foundation
import UIKit

class PokedexViewControler: UIViewController,UIWebViewDelegate,UITableViewDataSource,PokemonServiceDelegate {
      var PokemonIndex = 0
    @IBOutlet weak var PokedexViewControler: UITableView!
    var pokemoArray:[Pokemon] = []
    
    override func viewDidLoad() {
     super.viewDidLoad()
    
         let service = PokemonService()
        service.delegate=self
        service.downloadpokemon()
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func get20firstpokemos(pokemons: [Pokemon]){
       pokemoArray = pokemons
        PokedexViewControler.reloadData()
      
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //switch section {
        //case 0:
           // return 5
        //default:
           // return 10
       // }
        
    return pokemoArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       let cell = tableView.dequeueReusableCell(withIdentifier: "pokemonCell") as! PokemonTableViewCell
        cell.fillData(pokemon: pokemoArray[indexPath.row])
       //cell.textLabel?.text = "\(indexPath)"
       return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let pokemonDetail = segue.destination as! DetaillViewController
        pokemonDetail.pokemon = pokemoArray[PokemonIndex]
    }
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        switch section {
        case 0:
            return "sección 1"
        default:
              return "sección 2"
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> IndexPath? {
            PokemonIndex = indexPath.row
            return indexPath
        }
        
        
    
        
    
    }
    
}
