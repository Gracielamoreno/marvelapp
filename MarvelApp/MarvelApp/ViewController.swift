
//
//  ViewController.swift
//  MarvelApp
//
//  Created by Graciela Moreno on 28/11/17.
//  Copyright © 2017 Graciela Moreno. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class ViewController: UIViewController {

    @IBOutlet weak var altura: UILabel!
    @IBOutlet weak var peso: UILabel!
    @IBOutlet weak var name: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
   
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func consultar(_ sender: UIButton) {
        
        Alamofire.request("https://pokeapi.co/api/v2/pokemon/1").responseObject  {(response:DataResponse< Pokemon >) in
            
            let pokemon = response.result.value
            
            DispatchQueue.main.async {
                
                self.name.text = pokemon?.name ?? ""
                self.altura.text = "\(pokemon?.heigth ?? 0)"
              self.peso.text = "\(pokemon?.weight ?? 0)"
            
        }
    }
    
}
}

